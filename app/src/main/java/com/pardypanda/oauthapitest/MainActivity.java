package com.pardypanda.oauthapitest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import se.akerfeldt.okhttp.signpost.OkHttpOAuthConsumer;
import se.akerfeldt.okhttp.signpost.SigningInterceptor;

public class MainActivity extends AppCompatActivity {

    public static final MediaType MEDIA_TYPE_MARKDOWN
            = MediaType.parse("text/x-markdown; charset=utf-8");
    private final String CONSUMER_KEY = "Q7DPGQ8V8HPU8D14CMIA";
    private final String CONSUMER_SECRET = "TMZC4D4QICJYUVLB6SZ9TIK0Z4EJA2FFBMAWCE4C";
    private final String token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJjbGllbnRJZCI6IlE3RFBHUThWOEhQVThEMTRDTUlBIiwiY2xpZW50U2VjcmV0IjoiVE1aQzRENFFJQ0pZVVZMQjZTWjlUSUswWjRFSkEyRkZCTUFXQ0U0QyIsInJlZGlyZWN0VXJpIjoiaHR0cDovL2xvY2FsaG9zdC9DYWxsYmFjayIsInVzZXJfaWQiOiIwMmRkNGRlOC0zNTcwLTQ5YWYtODczZS0wYWU1YzZjYTMwNGYiLCJpYXQiOjE0NTA1MjcxNTl9.bam_4uj7iYPI2dSi7WR-91-dcHOJbzqOQBELY4AkT_aXW_sTj3Q1Pdcead_DGPTEowGaPYzHfF6O6LAbKskpepsaeEJF5tbTEkucT1wFX3GklgMr26Wp3bnkXteKIPD-HqbW2RURxJjIuLk3-iUdmGeOzHi2vF07oKgMAUAaz_4";
    TextView txtString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtString = (TextView) findViewById(R.id.txtString);
        txtString.setText("Trying signup API with ");

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url("http://analyzer.storedock.in:1800/v2/stores/")
                .header("x-authenticated", "oauth2")
                .header("authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJjbGllbnRJZCI6IlE3RFBHUThWOEhQVThEMTRDTUlBIiwiY2xpZW50U2VjcmV0IjoiVE1aQzRENFFJQ0pZVVZMQjZTWjlUSUswWjRFSkEyRkZCTUFXQ0U0QyIsInJlZGlyZWN0VXJpIjoiaHR0cDovL2xvY2FsaG9zdC9DYWxsYmFjayIsInVzZXJfaWQiOiIwMmRkNGRlOC0zNTcwLTQ5YWYtODczZS0wYWU1YzZjYTMwNGYiLCJpYXQiOjE0NTA1MjcxNTl9.bam_4uj7iYPI2dSi7WR-91-dcHOJbzqOQBELY4AkT_aXW_sTj3Q1Pdcead_DGPTEowGaPYzHfF6O6LAbKskpepsaeEJF5tbTEkucT1wFX3GklgMr26Wp3bnkXteKIPD-HqbW2RURxJjIuLk3-iUdmGeOzHi2vF07oKgMAUAaz_4")
                .header("Content-Type", "application/json")
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN, getDataEnd()))
                .build();

//        OkHttpOAuthConsumer consumer = new OkHttpOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
//        consumer.setTokenWithSecret(token, secret);
//        client.interceptors().add(new SigningInterceptor(consumer));

        client.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(Request request, IOException e) {
                txtString.setText(e.getLocalizedMessage());
                e.printStackTrace();
            }

            @Override
            public void onResponse(final Response response) throws IOException {
                // ... check for failure using `isSuccessful` before proceeding
                if (!response.isSuccessful()) {
                    txtString.setText(response.toString());
                    throw new IOException("Unexpected code " + response);
                }

                // Read data on the worker thread
                final String responseData = response.body().string();

                // Run view-related code back on the main thread
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            TextView myTextView = (TextView) findViewById(R.id.txtString);
                            myTextView.setText(responseData);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }


    public String getDataEnd() {
        JSONObject address = new JSONObject();
        try {
            address.put("line1", "Vadodra 1");
            address.put("line2", "Vadodra 2");
            address.put("city", "Vadodra");
            address.put("zipCode", "346387");
            address.put("mobile", "85188808491");
            address.put("countryCode", "IN");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONObject fullDAta = new JSONObject();
        try {
            fullDAta.put("name", "Ram ji Tiwari");
            fullDAta.put("model", "restaurant");
            fullDAta.put("billingCompanyName", "Acme Corp");
            fullDAta.put("address", address);
            Log.d("json", fullDAta.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return fullDAta.toString();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
